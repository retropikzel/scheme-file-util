(define-library
  (srfi 170)
  (import (scheme base)
          (scheme write))
  (export file-info-record
          file-info-record?
          file-info-directory?
          file-info
          directory-files)
  (begin

    (define-record-type file-info-record
      (make-file-info-record fname/port follow?)
      file-info-record?
      (fname/port file-info:fname/port)
      (follow? file-info:follow?))

    (define file-info
      (lambda (fname/port follow?)
        (if (port? fname/port)
          (error "file-info does not currently support ports"))
        (make-file-info-record fname/port follow?)))

    (define file-info-directory?
      (lambda (file-info)
        (let ((file (java.io.File (file-info:fname/port file-info))))
          (invoke file 'isDirectory))))

    (define directory-files
      (lambda (path)
        (let* ((file (java.io.File path))
               (file-list (file:list))
               (file-list-length file-list:length))
          (letrec ((looper
                     (lambda (index result)
                       (if (< index file-list-length)
                         (looper (+ index 1) (append result (list (file-list index))))
                         result))))
            (looper 0 (list))))))))
