# Ideas
## file-util-ensure-directory-exists rename
Rename this to file-util-mkdir. Keep the behavior as same. love.filesystem.mkdir does it too.

## Add love.filesystem
love2d has nice collection of filesystem util stuff, add same to this library.

https://love2d.org/wiki/love.filesystem

## Procedure: spit
Output string into file overwriting the contents.

## Procedure: slurp
Read whole file into string.

## Procedure: tree
Works like unix tree command, outputting file tree of given directory path.

