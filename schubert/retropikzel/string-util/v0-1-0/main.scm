(define-library
  (retropikzel string-util v0-1-0 main)
  (import (scheme base)
          (scheme write))
  (export string-util-split-by-char
          string-util-split-by-string
          string-util-contains-char?)
  (begin

    (define string-util-contains-char?
      (lambda (str mark)
        (cond
          ((= (string-length str) 0) #f)
          ((char=? mark (string-ref str 0)) #t)
          ((= (string-length str) 1) #f)
          (else (string-util-contains-char? (string-copy str 1) mark)))))

    (define string-util-split-by-char
      (lambda (str mark)
        (let* ((str-l (string->list str))
               (res (list))
               (last-index 0)
               (index 0)
               (splitter (lambda (c)
                           (cond ((char=? c mark)
                                  (begin
                                    (set! res (append res (list (substring str last-index index))))
                                    (set! last-index (+ index 1))))
                                 ((equal? (length str-l)  (+ index 1))
                                  (set! res (append res (list (substring str last-index (+ index 1)))))))
                           (set! index (+ index 1)))))
          (for-each splitter str-l)
          res)))

    (define string-util-split-by-string
      (lambda (str mark)
        (if (string=? mark "")
          str
          (letrec* ((mark-length (string-length mark))
                    (str-length (string-length str))
                    (index-end (- str-length mark-length))
                    (looper
                      (lambda (result index last-index)
                        (cond
                          ((= index str-length)
                           (append result
                                   (list (string-copy str last-index str-length))))
                          ((and (<= index index-end)
                                (string=? (string-copy str index (+ index mark-length)) mark))
                           (looper (append result (list (string-copy str last-index index)))
                                   (+ index mark-length)
                                   (+ index mark-length)))
                          (else (looper result (+ index 1) last-index))
                          ))))
            (looper (list) 0 0)

            ))))))
