(import (scheme base)
        (scheme write)
        (retropikzel file-util v0-2-0 main)
        (srfi 64))

(test-begin "slurp")

(test-equal "Hello world!\n\nThird line\n"
            (file-util-slurp "test/slurptestfile.txt"))

(test-end "slurp")
