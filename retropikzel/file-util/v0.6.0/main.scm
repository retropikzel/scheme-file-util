(define-library
  (retropikzel file-util v0.6.0 main)
  (import (scheme base)
          (scheme read)
          (scheme write)
          (scheme file)
          (retropikzel string-util v0.8.0 main)
          (srfi 170))
  (export file-util-copy
          file-util-copy-directory
          file-util-tree
          file-util-dirname
          file-util-ensure-directory-exists
          file-util-slurp
          file-util-spit
          file-util->list
          file-util-directory?
          file-util-list-files
          file-util-list-directories)
  (begin

    (define file-util-dirname
      (lambda (path)
        (string-append "/"
                       (string-util-join
                         (reverse
                           (list-tail
                             (reverse (string-util-split-by-char path #\/))
                             1))
                         "/"))))

    (define file-util-tree
      (lambda (path)
        (if (not (file-info-directory? (file-info path #f)))
          (list (string-copy path 2))
          (apply
            append
            (map tree
                 (map (lambda (filename)
                        (string-append path "/" filename))
                      (directory-files path)))))))

    (define file-util-ensure-directory-exists
      (lambda (directory)
        (letrec ((split-path (string-util-split-by-char directory #\/))
                 (looper (lambda (args path)
                           (if (and (not (string=? path ""))
                                    (not (file-exists? path)))
                             (create-directory path))
                           (if (not (null? args))
                             (looper (cdr args) (string-append path "/" (car args)))))))
          (if (not (null? split-path))
            (looper (cdr split-path) (car split-path))))))

    (define file-util-copy
      (lambda (source-path destination-path)
        (let ((source-port (open-binary-input-file source-path))
              (destination-port (open-binary-output-file destination-path)))
          (letrec ((loop (lambda (bytes)
                           (if (not (eof-object? bytes))
                             (begin
                               (write-bytevector bytes
                                                 destination-port)
                               (loop (read-bytevector 20000 source-port)))))))
            (loop (read-bytevector 20000 source-port)))
          (close-port source-port)
          (close-port destination-port))))

    (define file-util-copy-directory
      (lambda (from to)
        (file-util-ensure-directory-exists to)
        (map
          (lambda (file)
            (let* ((from-path (string-append from "/" file))
                   (to-path (string-append to "/" file))
                   (is-directory? (file-info-directory? (file-info from-path #f))))
              (if is-directory?
                (begin
                  (file-util-ensure-directory-exists to-path)
                  (file-util-copy-directory from-path to-path))
                (begin
                  (file-util-copy from-path to-path)))))
          (directory-files from))))

    (define file-util-slurp
      (lambda (file)
        (utf8->string
          (with-input-from-file
            file
            (lambda ()
              (letrec ((looper (lambda (result)
                                 (let ((bytes (read-bytevector 4000)))
                                   (if (eof-object? bytes)
                                     result
                                     (looper (bytevector-append result bytes)))))))
                (looper (bytevector))))))))

    (define file-util-spit
      (lambda (content file)
        (let ((port (open-binary-output-file file)))
          (write-bytevector (string->utf8 content) port)
          (close-port port))))

    (define file-util->list
      (lambda (path)
        (with-input-from-file
          path
          (lambda ()
            (letrec ((looper (lambda (result line)
                               (if (eof-object? line)
                                 result
                                 (looper (append result (list line))
                                         (read-line))))))
              (looper (list) (read-line)))))))

    (define file-util-directory?
      (lambda (path)
        (file-info-directory? (file-info path #t))))

    (define file-util-list-files
      (lambda (path dotfiles?)
        (let ((result (list)))
          (for-each
            (lambda (file-name)
              (if (not (file-util-directory? (string-append path "/" file-name)))
                (set! result (append result (list file-name)))))
            (directory-files path dotfiles?))
          result)))

    (define file-util-list-directories
      (lambda (path dotfiles?)
        (let ((result (list)))
          (for-each
            (lambda (file-name)
              (if (file-util-directory? (string-append path "/" file-name))
                (set! result (append result (list file-name)))))
            (directory-files path dotfiles?))
          result)))

    ))
