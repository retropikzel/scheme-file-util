guix-shell:
	guix shell gauche kawa

run-tests:
	gosh -I . -I ./schubert -r7 test/tests.scm
	kawa --r7rs -Dkawa.import.path="$(shell pwd):$(shell pwd)/schubert" test/tests.scm

